package com.hoanglechanhtu.mavenproject;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;

public class Plane extends _3dObject {
	public float a;
	public float b;
	public float c;
	public float d;
	public Plane(String _name){
		super();
		name = _name;
		a=0;
		b=1;
		c=0;
		d=1;
	}
	public void draw(GLAutoDrawable drawable){
		
		 final GL2 gl = drawable.getGL().getGL2();
		   
		   
		   gl.glBegin(GL2.GL_LINE_LOOP); // Start Drawing The Cube
		      gl.glColor3f(1f,0f,0f); //red color
		      gl.glVertex3f(1.5f, -1.0f, -1.5f); // Top Right Of The Quad (Top)
		      gl.glVertex3f( -1.5f, -1.0f, -1.5f); // Top Left Of The Quad (Top)
		      gl.glVertex3f( -1.5f, -1.0f, 1.5f ); // Bottom Left Of The Quad (Top)
		      gl.glVertex3f( 1.5f, -1.0f, 1.5f ); // Bottom Right Of The Quad (Top)
		   gl.glEnd();
		   gl.glPopMatrix();
	}
}
