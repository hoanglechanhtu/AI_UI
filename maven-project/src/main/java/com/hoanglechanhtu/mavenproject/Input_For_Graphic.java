
package com.hoanglechanhtu.mavenproject;

import java.awt.EventQueue;
import java.awt.Font;
import java.util.Arrays;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.JScrollPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;


//////// FOL package

import aima.core.logic.fol.domain.FOLDomain;
import aima.core.logic.fol.inference.FOLFCAsk;
import aima.core.logic.fol.inference.FOLTFMResolution;
import aima.core.logic.fol.inference.FOLBCAsk;
import aima.core.logic.fol.inference.InferenceProcedure;
import aima.core.logic.fol.inference.InferenceResult;
import aima.core.logic.fol.inference.proof.Proof;
import aima.core.logic.fol.inference.proof.ProofPrinter;
import aima.core.logic.fol.parsing.FOLParser;
import aima.core.logic.fol.parsing.ast.Constant;
import aima.core.logic.fol.parsing.ast.Predicate;
import aima.core.logic.fol.parsing.ast.Sentence;
import aima.core.logic.fol.parsing.ast.Term;
import aima.core.logic.fol.parsing.ast.Variable;

// domain

import aima.core.logic.fol.kb.FOLKnowledgeBase;

public class Input_For_Graphic extends JFrame {

	private JPanel contentPane;
	private JTextField textNewLine;
	private JTextField textNewPredicate;
	private JTextField textNewPlane;
	private JTextField textQuery;
	private JTextField textNewRule;
	private Font font1 = new Font("SansSerif", Font.BOLD, 20);
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Input_For_Graphic a = new Input_For_Graphic();
					a.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void run() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Input_For_Graphic frame = new Input_For_Graphic();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	// support function ///////////////////////////////////////////////////////////////////////////
	
	// insert a specific element into array
	public static Boolean insertElementIntoArray(String[] myArray, int[] size, String value) {
		
		// condition
		if(size[0] > myArray.length) {
			JOptionPane.showMessageDialog(null, "Number of element is big");
			return false;
		}
		
		if(value.equals("")) {
			JOptionPane.showMessageDialog(null, "Cannot add an empty value");
			return false;
		}
		
		// check if value is already exist or not
		for(int i = 0; i<size[0]; i++) {
			if(myArray[i].equals(value)) {
				JOptionPane.showMessageDialog(null, "Element " + value + " is already exist");
				return false;
			}
		}
		
		// add value to the end of the array
		size[0] += 1;
		myArray[size[0] - 1] = value;
		return true;
	}
	
	// remove a specific element out of array
	public static Boolean removeElementOutOfArray(String[] myArray, int[] size, String value) {
		// condition
		if(size[0] <= 0) {
			JOptionPane.showMessageDialog(null, "Number of element is zero");
			return false;
		}
		
		if(value.equals("")) {
			JOptionPane.showMessageDialog(null, "Cannot delete an empty value");
			return false;
		}
		// check if value is exist or not
		// if value is exist, delete it
		for(int i = 0; i<size[0]; i++) {
			if(myArray[i].equals(value)) {
				size[0] -= 1;
				if(i == size[0]) {return true;}
				else {
					for(int j = i; j < size[0]; j ++) {
						myArray[i] = myArray[i+1];
					}
					return true;
				}
			}
		}
		
		// return false because nothing is deleted
		JOptionPane.showMessageDialog(null, "Cannot find " + value + " element");
		return false;
	}
	
	
	// create a new domain
	public static FOLDomain createDomain(String[] Line, String[] Plane, String[] predicate, int[] lineSize, int[] planeSize, int[] predicateSize) {
		FOLDomain _domain = new FOLDomain();
		try {
			// insert Line into domain
			if(lineSize[0] > 0) {
				for(int i = 0; i < lineSize[0]; i++) {
					_domain.addConstant(Line[i]);
				}
			}
			
			// insert function into domain
			if(planeSize[0] > 0) {
				for(int i = 0; i < planeSize[0]; i++) {
					_domain.addConstant(Plane[i]);
				}
			}
			
			// insert predicate into domain
			if(predicateSize[0] > 0) {
				for(int i = 0; i < predicateSize[0]; i++) {
					_domain.addPredicate(predicate[i]);
				}
			}
		} catch(Exception e) {
			JOptionPane.showMessageDialog(null, "Something is wrong!");
		}
		// end create domain
		return _domain;
		
	}
	
	// create knowledge base
	public static FOLKnowledgeBase createFOLKnowledgeBase(InferenceProcedure infp, FOLDomain _domain, String[] rule, int[] ruleSize) {
		
		FOLKnowledgeBase kb = new FOLKnowledgeBase(_domain, infp);
		
		for(int i = 0; i < ruleSize[0]; i ++) {
			try {
				kb.tell(rule[i]);
			}
			catch(Exception e) {
				JOptionPane.showMessageDialog(null, "Something went wrong with rule " + rule[i]);
			}
		}
		return kb;
		
	}
	
	// solve forward chaining and backward chaining
	public static String chainingSolve(FOLDomain domain, FOLKnowledgeBase kb, JTextField textQuery) {
		
		// FOLParser parser = new FOLParser(domain);
		
		// take query
		String kbStr = kb.toString();
		String myQuery = textQuery.getText();
		// JOptionPane.showMessageDialog(null, myQuery);
		
		
		String result = "";
		try {
			// Sentence  query = parser.parse(myQuery);
			
			InferenceResult answer = kb.ask(myQuery);
			
			//System.out.println("Knowledge Base");
			result += "Knowledge Base: " + "\n" ;
			result += kbStr + "\n";
			result += "\n";
			// System.out.println(kbStr);
			result += "Query: " + myQuery + "\n" + "\n";
			//System.out.println("Query: " + query);
			result += "result: \n";
			for (Proof p : answer.getProofs()) {
				//System.out.print(ProofPrinter.printProof(p));
				//System.out.println("");
				result += ProofPrinter.printProof(p) + "\n";
			}
			return result;
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, "wrong query");
			return "";
		}
	}
	
	
	//Solving by resolution
	public static String  resolutionSolve(FOLDomain domain, FOLKnowledgeBase kb, JTextField textQuery) {
		
		// FOLParser parser = new FOLParser(domain);
		
		// take query
		String kbStr = kb.toString();
		String myQuery = textQuery.getText().trim();
		// JOptionPane.showMessageDialog(null, myQuery);
		
		
		String result = "";
		try {
			// Sentence  query = parser.parse(myQuery);
			
			InferenceResult answer = kb.ask(myQuery);
			
			//System.out.println("Knowledge Base");
			result += "Knowledge Base: " + "\n" ;
			result += kbStr + "\n";
			result += "\n";
			// System.out.println(kbStr);
			result += "Query: " + myQuery + "\n" + "\n";
			//System.out.println("Query: " + query);
			for (Proof p : answer.getProofs()) {
				//System.out.print(ProofPrinter.printProof(p));
				//System.out.println("");
				result += ProofPrinter.printProof(p) + "\n";
			}
			return result;
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, "wrong query");
			return "";
		}
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////
	

	/**
	 * Create the frame.
	 */
	public Input_For_Graphic() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		
		// global variable  //////////////////////////////////////////////
		final String []line = new String[100];
		Arrays.fill(line, "");
		final int[] lineSize = new int[1];
		lineSize[0] = 0;
		
		final String []plane= new String[100];
		Arrays.fill(plane, "");
		final int[] planeSize = new int[1];
		planeSize[0] = 0;
		
		final String []predicate = new String[100];
		Arrays.fill(predicate, "");
		final int[] predicateSize = new int[1];
		predicateSize[0] = 0;
		
		final String []rule = new String[100];
		Arrays.fill(rule, "");
		final int[] ruleSize = new int[1];
		ruleSize[0] = 0;
		/////////////////////////////////////////////////////////////////
		
		// show output //////////////////////////////////////////////////
		// constants output
		
		final JTextPane txtpnLine = new JTextPane();
		JScrollPane _txtScrollLine = new JScrollPane(txtpnLine);
		_txtScrollLine.setBounds(25, 21, 272, 231);
		txtpnLine.setBounds(25, 21, 272, 231);
		contentPane.add(_txtScrollLine);
		txtpnLine.setFont(font1);		
		// functions output
		
		final JTextPane txtpnPlane = new JTextPane();
		JScrollPane _txtScrollPlane = new JScrollPane(txtpnPlane);
		_txtScrollPlane.setBounds(372, 21, 272, 231);
		txtpnPlane.setBounds(372, 21, 272, 231);
		txtpnPlane.setFont(font1);
		contentPane.add(_txtScrollPlane);
				
		// predicates output
		final JTextPane txtpnPredicate = new JTextPane();
		JScrollPane _txtScrollPredicate = new JScrollPane(txtpnPredicate);
		_txtScrollPredicate.setBounds(690, 21, 272, 231);
		txtpnPredicate.setBounds(690, 21, 272, 231);
		contentPane.add(_txtScrollPredicate);	
		txtpnPredicate.setFont(font1);
		
		
		// rules output
		final JTextPane txtpnRule = new JTextPane();
		JScrollPane _txtScrollRule = new JScrollPane(txtpnRule);
		_txtScrollRule.setBounds(25, 367, 505, 196);
		txtpnRule.setBounds(25, 367, 505, 196);
		contentPane.add(_txtScrollRule);
		txtpnRule.setFont(font1);
		//////////////////////////////////////////////////////////////////////////////////
		
		
		//  constant ////////////////////////////////////////////////////
		
		// text field
		textNewLine = new JTextField();
		textNewLine.setFont(font1);
		textNewLine.setBounds(74, 263, 182, 30);
		textNewLine.add(textNewLine);
		textNewLine.setColumns(10);
		ActionListener textNewLineAction = new  ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				// get value from text field
				String newLine = textNewLine.getText().trim();
				
				// insert
				Boolean isSuccess = insertElementIntoArray(line, lineSize, newLine);
				
				// show result
				if(isSuccess) {
					String result = "";
					for(int i = 0; i < lineSize[0]; i++) {
						result += line[i] + "\t";
					}
					
					txtpnLine.setText(result);
				}
				textNewLine.setText("");
			}
		};
		//Add action listener to text field
		textNewLine.addActionListener(textNewLineAction);
		// add button
		JButton btnAddLine = new JButton("ADD Line");
		btnAddLine.addActionListener(textNewLineAction);
		btnAddLine.setBounds(10, 304, 140, 30);
		contentPane.add(btnAddLine);
		
		// remove button
		JButton btnRemoveLine = new JButton("REMOVE Line");
		btnRemoveLine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				// get value from text field
				String newLine = textNewLine.getText().trim();
				
				// remove
				Boolean isSuccess = removeElementOutOfArray(line, lineSize, newLine);
				
				// show result
				if(isSuccess) {
					String result = "";
					for(int i = 0; i < lineSize[0]; i++) {
						result += line[i] + "\t";
					}
					
					txtpnLine.setText(result);
				}
				textNewLine.setText("");

			}
		});
		btnRemoveLine.setBounds(160, 304, 161, 30);
		contentPane.add(btnRemoveLine);
		
		
		///////////////////////////////////////////////////////////////////
		
		
		
		//  predicate /////////////////////////////////////////////////////
		
		// text field
		textNewPredicate = new JTextField();
		textNewPredicate.setFont(font1);
		textNewPredicate.setColumns(10);
		textNewPredicate.setBounds(736, 263, 182, 30);
		contentPane.add(textNewPredicate);
		ActionListener textNewPreAction = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// get value from text field
				String newPredicate = textNewPredicate.getText().trim();
				
				// insert
				Boolean isSuccess = insertElementIntoArray(predicate, predicateSize, newPredicate);
				
				// show result
				if(isSuccess) {
					String result = "";
					for(int i = 0; i < predicateSize[0]; i++) {
						result += predicate[i] + "\t";
					}
					
					txtpnPredicate.setText(result);
				}
				textNewPredicate.setText("");
			}
		};
		//
		textNewPredicate.addActionListener(textNewPreAction);
		// add button
		JButton btnAddPredicate = new JButton("ADD PREDICATE");
		btnAddPredicate.addActionListener(textNewPreAction);
		btnAddPredicate.setBounds(670, 304, 133, 30);
		contentPane.add(btnAddPredicate);
		
		// remove button
		JButton btnRemovePredicate = new JButton("REMOVE PREDICATE");
		btnRemovePredicate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// get value from text field
				String newPredicate = textNewPredicate.getText().trim();
				
				// remove
				Boolean isSuccess = removeElementOutOfArray(predicate, predicateSize, newPredicate);
						
				// show result
				if(isSuccess) {
					String result = "";
					for(int i = 0; i < predicateSize[0]; i++) {
						result += predicate[i] + "\t";
					}
					
					txtpnPredicate.setText(result);
				}
				textNewPredicate.setText("");
			}
		});
		btnRemovePredicate.setBounds(813, 304, 161, 30);
		contentPane.add(btnRemovePredicate);
				
		/////////////////////////////////////////////////////////////////////
		
		
		//  plane ///////////////////////////////////////////////////////
		// text field
		textNewPlane = new JTextField();
		textNewPlane.setFont(font1);
		textNewPlane.setColumns(10);
		textNewPlane.setBounds(419, 263, 182, 30);
		contentPane.add(textNewPlane);
		ActionListener newPlaneAction = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// get value from text field
				String newPlane = textNewPlane.getText().trim();
				
				// insert
				Boolean isSuccess = insertElementIntoArray(plane, planeSize, newPlane);
				
				// show result
				if(isSuccess) {
					String result = "";
					for(int i = 0; i < planeSize[0]; i++) {
						result += plane[i] + "\t";
					}
					
					txtpnPlane.setText(result);
				}
				textNewPlane.setText("");
			}
		};
		
		//
		textNewPlane.addActionListener(newPlaneAction);
		// add button
		
		
		JButton btnAddPlane = new JButton("ADD PLANE");
		btnAddPlane.addActionListener(newPlaneAction);
		btnAddPlane.setBounds(356, 304, 133, 30);
		contentPane.add(btnAddPlane);

		// remove button

		JButton btnRemovePlane = new JButton("REMOVE PLANE");
		btnRemovePlane.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// get value from text field
				String newPlane = textNewPlane.getText().trim();
				
				// remove
				Boolean isSuccess = removeElementOutOfArray(plane, planeSize, newPlane);
				
				// show result
				if(isSuccess) {
					String result = "";
					for(int i = 0; i < planeSize[0]; i++) {
						result += plane[i] + "\t";
					}
					
					txtpnPlane.setText(result);
				}
				textNewPlane.setText("");
			}
		});
		btnRemovePlane.setBounds(496, 304, 148, 30);
		contentPane.add(btnRemovePlane);
		

		//////////////////////////////////////////////////////////////
		
		//  rule ///////////////////////////////////////////////////////
		
		// text field
		textNewRule = new JTextField();
		textNewRule.setFont(font1);
		textNewRule.setColumns(10);
		textNewRule.setBounds(25, 574, 505, 30);
		contentPane.add(textNewRule);
		ActionListener newRuleAction = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// get value from text field
				String newRule = textNewRule.getText().trim();
				
				// insert
				Boolean isSuccess = insertElementIntoArray(rule, ruleSize, newRule);
				
				// show result
				if(isSuccess) {
					String result = "";
					for(int i = 0; i < ruleSize[0]; i++) {
						result += rule[i] + "\n";
					}
					
					txtpnRule.setText(result);
				}
				textNewRule.setText("");
			}
		};
		
		textNewRule.addActionListener(newRuleAction);
		// add button
		JButton btnAddRule = new JButton("ADD RULE");
		btnAddRule.addActionListener(newRuleAction);
		btnAddRule.setBounds(74, 620, 182, 30);
		contentPane.add(btnAddRule);
		
		// remove button
		JButton btnRemoveRule = new JButton("REMOVE RULE");
		btnRemoveRule.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// get value from text field
				String newRule = textNewRule.getText().trim();
				
				// insert
				Boolean isSuccess = removeElementOutOfArray(rule, ruleSize, newRule);
				
				// show result
				if(isSuccess) {
					String result = "";
					for(int i = 0; i < ruleSize[0]; i++) {
						result += rule[i] + "\n";
					}
					
					txtpnRule.setText(result);
				}
				textNewRule.setText("");
			}
		});
		btnRemoveRule.setBounds(276, 620, 182, 30);
		contentPane.add(btnRemoveRule);
		///////////////////////////////////////////////////////////////
		
		
		// Query text label
		JLabel lblQuery = new JLabel("Query");
		lblQuery.setBounds(584, 445, 48, 30);
		contentPane.add(lblQuery);
		
		// Query text field
		textQuery = new JTextField();
		textQuery.setFont(font1);
		textQuery.setColumns(10);
		textQuery.setBounds(642, 445, 297, 30);
		contentPane.add(textQuery);
		
		// click event to solve and illustrate result
		JButton btnForwardChaining = new JButton("FORWARD CHAINING");
		btnForwardChaining.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				// create domain from line, function and predicate
				FOLDomain domain = createDomain(line, plane, predicate, lineSize, planeSize, predicateSize);
				
				InferenceProcedure infp = new FOLFCAsk();
				// create a fol knowledge base
				FOLKnowledgeBase kb = createFOLKnowledgeBase(infp, domain, rule, ruleSize);
				
				
				// chainingSolve(infp, domain, kb, textQuery);
				String title = "Forward chaining \n \n";
				try {
					ResultWindow frame = new ResultWindow(title + chainingSolve(domain, kb, textQuery));
					frame.setVisible(true);
				} catch (Exception k) {
					k.printStackTrace();
				}
				
			}
		});
		btnForwardChaining.setBounds(605, 486, 161, 30);
		contentPane.add(btnForwardChaining);
	
		
		JButton btnBackwardChaining = new JButton("BACKWARD CHAINING");
		btnBackwardChaining.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// create domain from line, function and predicate
				FOLDomain domain = createDomain(line, plane, predicate, lineSize, planeSize, predicateSize);
				
				InferenceProcedure infp = new FOLBCAsk();
				// create a fol knowledge base
				FOLKnowledgeBase kb = createFOLKnowledgeBase(infp, domain, rule, ruleSize);
				/*
				// after create domain
				System.out.println("here");
				String kbStr = kb.toString();
				System.out.println(kbStr);
				System.out.println("here");
				*/
				
				//chainingSolve(infp, domain, kb, textQuery);
				String title = "Backward chaining \n \n";
				try {
					ResultWindow frame = new ResultWindow(title + chainingSolve(domain, kb, textQuery));
					frame.setVisible(true);
				} catch (Exception k) {
					k.printStackTrace();
				}
			}
		});
		btnBackwardChaining.setBounds(786, 486, 176, 30);
		contentPane.add(btnBackwardChaining);
		
		
		
		///////////////////////////////////////////////////////////////
		
		// click event to solve and illustrate result
				JButton btnSolve = new JButton("SOLVE");
				btnSolve.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
					
						// create domain from line, function and predicate
						FOLDomain domain = createDomain(line, plane, predicate, lineSize, planeSize, predicateSize);
						
						InferenceProcedure infp = new FOLTFMResolution();
						// create a fol knowledge base
						FOLKnowledgeBase kb = createFOLKnowledgeBase(infp, domain, rule, ruleSize);
						
						
						// chainingSolve(infp, domain, kb, textQuery);
						String title = "Forward chaining \n \n";
						try {
							ResultWindow frame = new ResultWindow(title + resolutionSolve(domain, kb, textQuery));
							frame.setVisible(true);
						} catch (Exception k) {
							k.printStackTrace();
						}
						
					}
				});
				btnSolve.setBounds(605, 550, 161, 30);
				contentPane.add(btnSolve);
		/////////////////////////////////////////////////////////////
				JButton btnUnifier = new JButton("Unifier");
				btnUnifier.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
					
						// create domain from line, function and predicate
						FOLDomain domain = createDomain(line, plane, predicate, lineSize, planeSize, predicateSize);
						
						InferenceProcedure infp = new FOLTFMResolution();
						// create a fol knowledge base
						FOLKnowledgeBase kb = createFOLKnowledgeBase(infp, domain, rule, ruleSize);
						
						
						// chainingSolve(infp, domain, kb, textQuery);
						String title = "Forward chaining \n \n";
						try {
							Unifier_UI_Demo frame = new Unifier_UI_Demo(domain);
							frame.setVisible(true);
						} catch (Exception k) {
							k.printStackTrace();
						}
						
					}
				});
				btnUnifier.setBounds(786, 550, 161, 30);
				contentPane.add(btnUnifier);
				////////////////////////
				JButton btnNew = new JButton("New");
				btnNew.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
					
						lineSize[0]=0;
						txtpnLine.setText("");
						predicateSize[0]=0;	
						txtpnPredicate.setText("");
						ruleSize[0]=0;
						txtpnRule.setText("");
						planeSize[0]=0;
						txtpnPlane.setText("");
						}
				});
				btnNew.setBounds(786, 600, 161, 30);
				contentPane.add(btnNew);
	}
}

