package com.hoanglechanhtu.mavenproject;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.JScrollBar;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.Box;

//////// FOL package
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import aima.core.logic.fol.StandardizeApartIndexicalFactory;
import aima.core.logic.fol.Unifier;
import aima.core.logic.fol.domain.DomainFactory;
import aima.core.logic.fol.domain.FOLDomain;
import aima.core.logic.fol.inference.FOLFCAsk;
import aima.core.logic.fol.inference.FOLTFMResolution;
import aima.core.logic.fol.inference.FOLBCAsk;
import aima.core.logic.fol.inference.InferenceProcedure;
import aima.core.logic.fol.inference.InferenceResult;
import aima.core.logic.fol.inference.proof.Proof;
import aima.core.logic.fol.inference.proof.ProofPrinter;
import aima.core.logic.fol.parsing.FOLParser;
import aima.core.logic.fol.parsing.ast.Constant;
import aima.core.logic.fol.parsing.ast.Predicate;
import aima.core.logic.fol.parsing.ast.Sentence;
import aima.core.logic.fol.parsing.ast.Term;
import aima.core.logic.fol.parsing.ast.Variable;

// domain
import aima.core.logic.fol.domain.FOLDomain;
import aima.core.logic.fol.kb.FOLKnowledgeBase;
import aima.core.logic.fol.kb.FOLKnowledgeBaseFactory;

public class Resolution_UI extends JFrame {

	private JPanel contentPane;
	private JTextField textNewConstant;
	private JTextField textNewPredicate;
	private JTextField textNewFunction;
	private JTextField textQuery;
	private JTextField textNewRule;
	private Font font1 = new Font("SansSerif", Font.BOLD, 20);
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Resolution_UI frame = new Resolution_UI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	// support function ///////////////////////////////////////////////////////////////////////////
	
	// insert a specific element into array
	public static Boolean insertElementIntoArray(String[] myArray, int[] size, String value) {
		
		// condition
		if(size[0] > myArray.length) {
			JOptionPane.showMessageDialog(null, "Number of element is big");
			return false;
		}
		
		if(value.equals("")) {
			JOptionPane.showMessageDialog(null, "Cannot add an empty value");
			return false;
		}
		
		// check if value is already exist or not
		for(int i = 0; i<size[0]; i++) {
			if(myArray[i].equals(value)) {
				JOptionPane.showMessageDialog(null, "Element " + value + " is already exist");
				return false;
			}
		}
		
		// add value to the end of the array
		size[0] += 1;
		myArray[size[0] - 1] = value;
		return true;
	}
	
	// remove a specific element out of array
	public static Boolean removeElementOutOfArray(String[] myArray, int[] size, String value) {
		// condition
		if(size[0] <= 0) {
			JOptionPane.showMessageDialog(null, "Number of element is zero");
			return false;
		}
		
		if(value.equals("")) {
			JOptionPane.showMessageDialog(null, "Cannot delete an empty value");
			return false;
		}
		// check if value is exist or not
		// if value is exist, delete it
		for(int i = 0; i<size[0]; i++) {
			if(myArray[i].equals(value)) {
				size[0] -= 1;
				if(i == size[0]) {return true;}
				else {
					for(int j = i; j < size[0]; j ++) {
						myArray[i] = myArray[i+1];
					}
					return true;
				}
			}
		}
		
		// return false because nothing is deleted
		JOptionPane.showMessageDialog(null, "Cannot find " + value + " element");
		return false;
	}
	
	
	// create a new domain
	public static FOLDomain createDomain(String[] constant, String[] function, String[] predicate, int[] constantSize, int[] functionSize, int[] predicateSize) {
		FOLDomain _domain = new FOLDomain();
		try {
			// insert constant into domain
			if(constantSize[0] > 0) {
				for(int i = 0; i < constantSize[0]; i++) {
					_domain.addConstant(constant[i]);
				}
			}
			
			// insert function into domain
			if(functionSize[0] > 0) {
				for(int i = 0; i < functionSize[0]; i++) {
					_domain.addFunction(function[i]);
				}
			}
			
			// insert predicate into domain
			if(predicateSize[0] > 0) {
				for(int i = 0; i < predicateSize[0]; i++) {
					_domain.addPredicate(predicate[i]);
				}
			}
		} catch(Exception e) {
			JOptionPane.showMessageDialog(null, "Something is wrong!");
		}
		// end create domain
		return _domain;
		
	}
	
	// create knowledge base
	public static FOLKnowledgeBase createFOLKnowledgeBase(InferenceProcedure infp, FOLDomain _domain, String[] rule, int[] ruleSize) {
		
		FOLKnowledgeBase kb = new FOLKnowledgeBase(_domain, infp);
		
		for(int i = 0; i < ruleSize[0]; i ++) {
			try {
				kb.tell(rule[i]);
			}
			catch(Exception e) {
				JOptionPane.showMessageDialog(null, "Something went wrong with rule " + rule[i]);
			}
		}
		return kb;
		
	}
	
	// solve forward chaining and backward chaining
	public static String resolutionSolve(FOLDomain domain, FOLKnowledgeBase kb, JTextField textQuery) {
		
		// FOLParser parser = new FOLParser(domain);
		
		// take query
		String kbStr = kb.toString();
		String myQuery = textQuery.getText().trim();
		// JOptionPane.showMessageDialog(null, myQuery);
		
		
		String result = "";
		try {
			// Sentence  query = parser.parse(myQuery);
			
			InferenceResult answer = kb.ask(myQuery);
			
			//System.out.println("Knowledge Base");
			result += "Knowledge Base: " + "\n" ;
			result += kbStr + "\n";
			result += "\n";
			// System.out.println(kbStr);
			result += "Query: " + myQuery + "\n" + "\n";
			//System.out.println("Query: " + query);
			for (Proof p : answer.getProofs()) {
				//System.out.print(ProofPrinter.printProof(p));
				//System.out.println("");
				result += ProofPrinter.printProof(p) + "\n";
			}
			return result;
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, "wrong query");
			return "";
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////
	

	/**
	 * Create the frame.
	 */
	public Resolution_UI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		
		// global variable  //////////////////////////////////////////////
		final String []constant = new String[100];
		Arrays.fill(constant, "");
		final int[] constantSize = new int[1];
		constantSize[0] = 0;
		
		final String []function = new String[100];
		Arrays.fill(function, "");
		final int[] functionSize = new int[1];
		functionSize[0] = 0;
		
		final String []predicate = new String[100];
		Arrays.fill(predicate, "");
		final int[] predicateSize = new int[1];
		predicateSize[0] = 0;
		
		final String []rule = new String[100];
		Arrays.fill(rule, "");
		final int[] ruleSize = new int[1];
		ruleSize[0] = 0;
		/////////////////////////////////////////////////////////////////
		
		// show output //////////////////////////////////////////////////
		// constants output
		final JTextPane txtpnConstant = new JTextPane();
		txtpnConstant.setBounds(25, 21, 272, 231);
		txtpnConstant.setFont(font1);
		contentPane.add(txtpnConstant);
				
		// functions output
		final JTextPane txtpnFunction = new JTextPane();
		txtpnFunction.setBounds(372, 21, 272, 231);
		txtpnFunction.setFont(font1);
		contentPane.add(txtpnFunction);
				
		// predicates output
		final JTextPane txtpnPredicate = new JTextPane();
		txtpnPredicate.setBounds(690, 21, 272, 231);
		txtpnPredicate.setFont(font1);
		contentPane.add(txtpnPredicate);	
		
		// rules output
		
		final JTextPane txtpnRule = new JTextPane();
		txtpnRule.setFont(font1);
		txtpnRule.setBounds(25, 367, 505, 196);
		contentPane.add(txtpnRule);
		//////////////////////////////////////////////////////////////////////////////////
		
		
		//  constant ////////////////////////////////////////////////////
		
		// text field
		textNewConstant = new JTextField();
		textNewConstant.setBounds(74, 263, 182, 30);
		textNewConstant.setFont(font1);
		contentPane.add(textNewConstant);
		textNewConstant.setColumns(10);
		
		// add button
		JButton btnAddConstant = new JButton("ADD CONSTANT");
		btnAddConstant.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				// get value from text field
				String newConstant = textNewConstant.getText().trim();
				
				// insert
				Boolean isSuccess = insertElementIntoArray(constant, constantSize, newConstant);
				
				// show result
				if(isSuccess) {
					String result = "";
					for(int i = 0; i < constantSize[0]; i++) {
						result += constant[i] + "\t";
					}
					
					txtpnConstant.setText(result);
				}
			}
		});
		btnAddConstant.setBounds(10, 304, 140, 30);
		contentPane.add(btnAddConstant);
		
		// remove button
		JButton btnRemoveConstant = new JButton("REMOVE CONSTANT");
		btnRemoveConstant.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				// get value from text field
				String newConstant = textNewConstant.getText().trim();
				
				// remove
				Boolean isSuccess = removeElementOutOfArray(constant, constantSize, newConstant);
				
				// show result
				if(isSuccess) {
					String result = "";
					for(int i = 0; i < constantSize[0]; i++) {
						result += constant[i] + "\t";
					}
					
					txtpnConstant.setText(result);
				}

			}
		});
		btnRemoveConstant.setBounds(160, 304, 161, 30);
		contentPane.add(btnRemoveConstant);
		
		
		///////////////////////////////////////////////////////////////////
		
		
		
		//  predicate /////////////////////////////////////////////////////
		
		// text field
		textNewPredicate = new JTextField();
		textNewPredicate.setColumns(10);
		textNewPredicate.setBounds(736, 263, 182, 30);
		textNewPredicate.setFont(font1);
		contentPane.add(textNewPredicate);
		
		// add button
		JButton btnAddPredicate = new JButton("ADD PREDICATE");
		btnAddPredicate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// get value from text field
				String newPredicate = textNewPredicate.getText().trim();
				
				// insert
				Boolean isSuccess = insertElementIntoArray(predicate, predicateSize, newPredicate);
				
				// show result
				if(isSuccess) {
					String result = "";
					for(int i = 0; i < predicateSize[0]; i++) {
						result += predicate[i] + "\t";
					}
					
					txtpnPredicate.setText(result);
				}
			}
		});
		btnAddPredicate.setBounds(670, 304, 133, 30);
		contentPane.add(btnAddPredicate);
		
		// remove button
		JButton btnRemovePredicate = new JButton("REMOVE PREDICATE");
		btnRemovePredicate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// get value from text field
				String newPredicate = textNewPredicate.getText().trim();
				
				// remove
				Boolean isSuccess = removeElementOutOfArray(predicate, predicateSize, newPredicate);
						
				// show result
				if(isSuccess) {
					String result = "";
					for(int i = 0; i < predicateSize[0]; i++) {
						result += predicate[i] + "\t";
					}
					
					txtpnPredicate.setText(result);
				}
			}
		});
		btnRemovePredicate.setBounds(813, 304, 161, 30);
		contentPane.add(btnRemovePredicate);
				
		/////////////////////////////////////////////////////////////////////
		
		
		//  function ///////////////////////////////////////////////////////
		// text field
		textNewFunction = new JTextField();
		textNewFunction.setColumns(10);
		textNewFunction.setBounds(419, 263, 182, 30);
		textNewFunction.setFont(font1);
		contentPane.add(textNewFunction);
		
		// add button
		JButton btnAddFunction = new JButton("ADD FUNCTION");
		btnAddFunction.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// get value from text field
				String newFunction = textNewFunction.getText().trim();
				
				// insert
				Boolean isSuccess = insertElementIntoArray(function, functionSize, newFunction);
				
				// show result
				if(isSuccess) {
					String result = "";
					for(int i = 0; i < functionSize[0]; i++) {
						result += function[i] + "\t";
					}
					
					txtpnFunction.setText(result);
				}
			}
		});
		btnAddFunction.setBounds(356, 304, 133, 30);
		contentPane.add(btnAddFunction);

		// remove button

		JButton btnRemoveFunction = new JButton("REMOVE FUNCTION");
		btnRemoveFunction.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// get value from text field
				String newFunction = textNewFunction.getText().trim();
				
				// remove
				Boolean isSuccess = removeElementOutOfArray(function, functionSize, newFunction);
				
				// show result
				if(isSuccess) {
					String result = "";
					for(int i = 0; i < functionSize[0]; i++) {
						result += function[i] + "\t";
					}
					
					txtpnFunction.setText(result);
				}
			}
		});
		btnRemoveFunction.setBounds(496, 304, 148, 30);
		contentPane.add(btnRemoveFunction);
		

		//////////////////////////////////////////////////////////////
		
		//  rule ///////////////////////////////////////////////////////
		
		// text field
		textNewRule = new JTextField();
		textNewRule.setColumns(10);
		textNewRule.setBounds(25, 574, 505, 30);
		textNewRule.setFont(font1);
		contentPane.add(textNewRule);
		
		// add button
		JButton btnAddRule = new JButton("ADD RULE");
		btnAddRule.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// get value from text field
				String newRule = textNewRule.getText().trim();
				
				// insert
				Boolean isSuccess = insertElementIntoArray(rule, ruleSize, newRule);
				
				// show result
				if(isSuccess) {
					String result = "";
					for(int i = 0; i < ruleSize[0]; i++) {
						result += rule[i] + "\n";
					}
					
					txtpnRule.setText(result);
				}
			}
		});
		btnAddRule.setBounds(74, 620, 182, 30);
		contentPane.add(btnAddRule);
		
		// remove button
		JButton btnRemoveRule = new JButton("REMOVE RULE");
		btnRemoveRule.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// get value from text field
				String newRule = textNewRule.getText().trim();
				
				// insert
				Boolean isSuccess = removeElementOutOfArray(rule, ruleSize, newRule);
				
				// show result
				if(isSuccess) {
					String result = "";
					for(int i = 0; i < ruleSize[0]; i++) {
						result += rule[i] + "\n";
					}
					
					txtpnRule.setText(result);
				}
			}
		});
		btnRemoveRule.setBounds(276, 620, 182, 30);
		contentPane.add(btnRemoveRule);
		///////////////////////////////////////////////////////////////
		
		
		// Query text label
		JLabel lblQuery = new JLabel("Query");
		lblQuery.setBounds(584, 445, 48, 30);
		contentPane.add(lblQuery);
		
		// Query text field
		textQuery = new JTextField();
		textQuery.setColumns(10);
		textQuery.setBounds(642, 445, 297, 30);
		textQuery.setFont(font1);
		contentPane.add(textQuery);
		
		// click event to solve and illustrate result
		JButton btnSolve = new JButton("SOLVE");
		btnSolve.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				// create domain from constant, function and predicate
				FOLDomain domain = createDomain(constant, function, predicate, constantSize, functionSize, predicateSize);
				
				InferenceProcedure infp = new FOLTFMResolution();
				// create a fol knowledge base
				FOLKnowledgeBase kb = createFOLKnowledgeBase(infp, domain, rule, ruleSize);
				
				
				// chainingSolve(infp, domain, kb, textQuery);
				String title = "Forward chaining \n \n";
				try {
					ResultWindow frame = new ResultWindow(title + resolutionSolve(domain, kb, textQuery));
					frame.setVisible(true);
				} catch (Exception k) {
					k.printStackTrace();
				}
				
			}
		});
		btnSolve.setBounds(712, 486, 161, 30);
		contentPane.add(btnSolve);
		
		
		
		///////////////////////////////////////////////////////////////
		
	}
}

