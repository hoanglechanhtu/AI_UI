package aima.gui.ai.present;

import java.util.ArrayList;
import java.util.List;

import aima.core.logic.fol.StandardizeApartIndexicalFactory;
import aima.core.logic.fol.inference.FOLBCAsk;
import aima.core.logic.fol.inference.InferenceProcedure;
import aima.core.logic.fol.inference.InferenceResult;
import aima.core.logic.fol.inference.proof.Proof;
import aima.core.logic.fol.inference.proof.ProofPrinter;
import aima.core.logic.fol.kb.FOLKnowledgeBase;
import aima.core.logic.fol.kb.FOLKnowledgeBaseFactory;
import aima.core.logic.fol.parsing.ast.Constant;
import aima.core.logic.fol.parsing.ast.Predicate;
import aima.core.logic.fol.parsing.ast.Term;
import aima.core.logic.fol.parsing.ast.Variable;

public class BackwardChaining {
	public static void main(String[] args) {
		fOL_bcAskDemo();
	}
	private static void fOL_bcAskDemo() {
		System.out.println("----------------------------");
		System.out.println("Backward Chain, Kings Demo 1");
		System.out.println("----------------------------");
		kingsDemo1(new FOLBCAsk());
		System.out.println("----------------------------");
		System.out.println("Backward Chain, Kings Demo 2");
		System.out.println("----------------------------");
		kingsDemo2(new FOLBCAsk());
		System.out.println("----------------------------");
		System.out.println("Backward Chain, Weapons Demo");
		System.out.println("----------------------------");
		weaponsDemo(new FOLBCAsk());
	}
	private static void kingsDemo1(InferenceProcedure ip) {
		StandardizeApartIndexicalFactory.flush();

		FOLKnowledgeBase kb = FOLKnowledgeBaseFactory
				.createKingsKnowledgeBase(ip);

		String kbStr = kb.toString();

		List<Term> terms = new ArrayList<Term>();
		terms.add(new Constant("John"));
		Predicate query = new Predicate("Evil", terms);

		InferenceResult answer = kb.ask(query);

		System.out.println("Kings Knowledge Base:");
		System.out.println(kbStr);
		System.out.println("Query: " + query);
		for (Proof p : answer.getProofs()) {
			System.out.print(ProofPrinter.printProof(p));
			System.out.println("");
		}
	}

	private static void kingsDemo2(InferenceProcedure ip) {
		StandardizeApartIndexicalFactory.flush();

		FOLKnowledgeBase kb = FOLKnowledgeBaseFactory
				.createKingsKnowledgeBase(ip);

		String kbStr = kb.toString();

		List<Term> terms = new ArrayList<Term>();
		terms.add(new Variable("x"));
		Predicate query = new Predicate("King", terms);

		InferenceResult answer = kb.ask(query);

		System.out.println("Kings Knowledge Base:");
		System.out.println(kbStr);
		System.out.println("Query: " + query);
		for (Proof p : answer.getProofs()) {
			System.out.print(ProofPrinter.printProof(p));
		}
	}

	private static void weaponsDemo(InferenceProcedure ip) {
		StandardizeApartIndexicalFactory.flush();

		FOLKnowledgeBase kb = FOLKnowledgeBaseFactory
				.createWeaponsKnowledgeBase(ip);

		String kbStr = kb.toString();

		List<Term> terms = new ArrayList<Term>();
		terms.add(new Variable("x"));
		Predicate query = new Predicate("Criminal", terms);

		InferenceResult answer = kb.ask(query);

		System.out.println("Weapons Knowledge Base:");
		System.out.println(kbStr);
		System.out.println("Query: " + query);
		for (Proof p : answer.getProofs()) {
			System.out.print(ProofPrinter.printProof(p));
			System.out.println("");
		}
	}

	
}
