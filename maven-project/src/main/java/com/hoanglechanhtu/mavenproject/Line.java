package com.hoanglechanhtu.mavenproject;

import java.util.Random;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;

public class Line extends _3dObject {
	 
	public float a;
	public float b;
	public float c;
	public float[][] orth = new float[10][3];
	public Line(String _name){
		super();
		name = _name;
		y=1;
		a= 1;
		b=0;
		c=1;
	}
	public void draw(GLAutoDrawable drawable){
		
		 final GL2 gl = drawable.getGL().getGL2();
		   
		   
		 gl.glBegin(GL2.GL_LINES); // Start Drawing The Cube
	      gl.glColor3f(0.3f,0.4f,0.1f); //red color
	      gl.glVertex3f(x, y, z); // Top Right Of The Quad (Top)
	      gl.glVertex3f( x+3*a, y+3*b, z+3*c); // Top Left Of The Quad (Top)
	      
	   gl.glEnd();
		 
	}
	public void calculateD(){
		if(r==0) return;
		if(r==1) {
			if(orth[0][0]==0.f){
				a=1;
				if(orth[0][1]==0.f){
					b=1;
					c=0;
				}
				else{
					c=1;
					b= -(orth[0][2])/orth[0][1];
				}
				
			}
			else{
				b=1;
				c=1;
				a = -(orth[0][1]+orth[0][2])/orth[0][0];
				
			}
		}
		else if(r==2){
			 
			b=1;
			float _a = (orth[1][0]-orth[0][0]);
			float _c =(orth[1][2]-orth[0][2]);
			c = orth[0][1]/(orth[0][0]*(-_c/_a)+orth[0][2]);
			a = (-_c/_a)*c;
		}
		if(a==-0)
			a=0;
		else if (b== -0) {
			b=0;			
		}
		else if(c==-0){
			c=0;
		}
	}
}
