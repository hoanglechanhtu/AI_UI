package com.hoanglechanhtu.mavenproject;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextPane;

public class ResultWindow extends JFrame {
	private Cube cube = new Cube();
	private JPanel contentPane;
	private JButton closeBtn;
	private Font font1 = new Font("SansSerif", Font.BOLD, 20);
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ResultWindow frame = new ResultWindow("Nothing here. Because there is no input.");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ResultWindow(String result) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(0, 0, 1000, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
	 
		JTextPane txtpnResult = new JTextPane();
		JScrollPane txtScrollResult = new JScrollPane(txtpnResult);
		txtpnResult.setFont(font1);
		txtScrollResult.setBounds(47, 31, 800, 600);
		txtpnResult.setBounds(47, 31, 600, 671);
		contentPane.add(txtScrollResult);
		
		txtpnResult.setText(result);
		cube.drawCube();
	}
}
