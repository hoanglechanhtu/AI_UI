package aima.gui.ai.present;

import aima.core.logic.fol.domain.FOLDomain;
import aima.core.logic.fol.kb.FOLKnowledgeBase;
import aima.core.logic.fol.inference.InferenceResult;

public class UseFOL {

	public static void main(String[] args) {
		FOLDomain domain = new FOLDomain();
		domain.addConstant("An");
		domain.addConstant("Quang");
		domain.addConstant("Tu");
		domain.addPredicate("pmh");
		domain.addPredicate("rich");
		domain.addPredicate("muchmoney");
		domain.addPredicate("govstaff");
		
		FOLKnowledgeBase myKB = new FOLKnowledgeBase(domain);
		myKB.tell("(FORALL x ((pmh(x) => rich(x))))");
		myKB.tell("(FORALL y ((rich(y) => muchmoney(y))))");
		myKB.tell("(govstaff(x) => (NOT muchmoney(x)) )");
		myKB.tell("(pmh(An))");
		myKB.tell("(rich(Quang))");
		myKB.tell("(govstaff(Tu))");
		String kbStr = myKB.toString();
		System.out.println(kbStr);
		
		//InferenceResult ir = myKB.ask("(muchmoney(x))");
		//System.out.println("IS TRUE = " + ir.getProofs());
	}

}
