package com.hoanglechanhtu.mavenproject;
import com.jogamp.opengl.GL2;


import com.jogamp.opengl.GLAutoDrawable;
 
import com.jogamp.opengl.GLCapabilities;
 
import com.jogamp.opengl.GLEventListener;
 
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.awt.GLJPanel;
import com.jogamp.opengl.glu.GLU;

import java.awt.DisplayMode;

import javax.swing.JFrame;
import com.jogamp.opengl.util.FPSAnimator;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.Box;

//////// FOL package
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import aima.core.agent.Action;
import aima.core.logic.common.Parser;
import aima.core.logic.fol.StandardizeApartIndexicalFactory;
import aima.core.logic.fol.Unifier;
import aima.core.logic.fol.domain.DomainFactory;
import aima.core.logic.fol.domain.FOLDomain;
import aima.core.logic.fol.inference.FOLFCAsk;
import aima.core.logic.fol.inference.FOLTFMResolution;
import aima.core.logic.fol.inference.FOLBCAsk;
import aima.core.logic.fol.inference.InferenceProcedure;
import aima.core.logic.fol.inference.InferenceResult;
import aima.core.logic.fol.inference.proof.Proof;
import aima.core.logic.fol.inference.proof.ProofPrinter;
import aima.core.logic.fol.parsing.FOLLexer;
import aima.core.logic.fol.parsing.FOLParser;
import aima.core.logic.fol.parsing.ast.Constant;
import aima.core.logic.fol.parsing.ast.Predicate;
import aima.core.logic.fol.parsing.ast.Sentence;
import aima.core.logic.fol.parsing.ast.Term;
import aima.core.logic.fol.parsing.ast.Variable;

// domain
import aima.core.logic.fol.domain.FOLDomain;
import aima.core.logic.fol.kb.FOLKnowledgeBase;
import aima.core.logic.fol.kb.FOLKnowledgeBaseFactory;

public class Forward_Backward_Chaining_UI_Demo extends JFrame {

	private JPanel contentPane;
	private JTextField textNewConstant;
	private JTextField textNewPredicate;
	private JTextField textNewFunction;
	private JTextField textQuery;
	private JTextField textNewRule;
	private Font font1 = new Font("SansSerif", Font.BOLD, 20);
	
	// global variable
	static final String []constant = new String[100];
	static final int[] constantSize = new int[1];
	static final String []function = new String[100];
	static final int[] functionSize = new int[1];
	static final String []predicate = new String[100];
	static final int[] predicateSize = new int[1];
	static final String []rule = new String[100];
	static final int[] ruleSize = new int[1];
	static final String[][] relation = new String[100][3];
	static final int[] relationSize = new int[1];
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Forward_Backward_Chaining_UI_Demo frame = new Forward_Backward_Chaining_UI_Demo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	// support function ///////////////////////////////////////////////////////////////////////////
	// true if a element is in a list, false if not
	public static Boolean isElementExistInList(Term element, String[] array, int[] arraySize, FOLParser parser) {
		for(int i=0; i < arraySize[0]; i++) {
			parser.setUpToParse(array[i]);
			Term c = parser.parseConstant();
			if(c.equals(element)) return true;
		}
		return false;
	}
	// insert a specific element into array
	public static Boolean insertElementIntoArray(String[] myArray, int[] size, String value) {
		
		// condition
		if(size[0] > myArray.length) {
			JOptionPane.showMessageDialog(null, "Number of element is big");
			return false;
		}
		
		if(value.equals("")) {
			JOptionPane.showMessageDialog(null, "Cannot add an empty value");
			return false;
		}
		
		// check if value is already exist or not
		for(int i = 0; i<size[0]; i++) {
			if(myArray[i].equals(value)) {
				JOptionPane.showMessageDialog(null, "Element " + value + " is already exist");
				return false;
			}
		}
		
		// add value to the end of the array
		size[0] += 1;
		myArray[size[0] - 1] = value;
		return true;
	}
	
	// remove a specific element out of array
	public static Boolean removeElementOutOfArray(String[] myArray, int[] size, String value) {
		// condition
		if(size[0] <= 0) {
			JOptionPane.showMessageDialog(null, "Number of element is zero");
			return false;
		}
		
		if(value.equals("")) {
			JOptionPane.showMessageDialog(null, "Cannot delete an empty value");
			return false;
		}
		// check if value is exist or not
		// if value is exist, delete it
		for(int i = 0; i<size[0]; i++) {
			if(myArray[i].equals(value)) {
				size[0] -= 1;
				if(i == size[0]) {return true;}
				else {
					for(int j = i; j < size[0]; j ++) {
						myArray[i] = myArray[i+1];
					}
					return true;
				}
			}
		}
		
		// return false because nothing is deleted
		JOptionPane.showMessageDialog(null, "Cannot find " + value + " element");
		return false;
	}
	
	
	// create a new domain
	public static FOLDomain createDomain(String[] line, String[] plane, String[] predicate, int[] lineSize, int[] planeSize, int[] predicateSize) {
		FOLDomain _domain = new FOLDomain();
		try {
			// insert constant into domain
			if(lineSize[0] > 0) {
				for(int i = 0; i < lineSize[0]; i++) {
					_domain.addConstant(line[i]);
				}
			}
			
			// insert function into domain
			if(planeSize[0] > 0) {
				for(int i = 0; i < planeSize[0]; i++) {
					_domain.addConstant(plane[i]);
				}
			}
			
			// insert predicate for our relation
			_domain.addPredicate("line_orth_plane");
			_domain.addPredicate("line_orth_line");
			_domain.addPredicate("plane_contain_line");
			_domain.addPredicate("line_diff_line");
			_domain.addPredicate("line_parallel_plane");
			
			// insert predicate into domain
			/*
			if(predicateSize[0] > 0) {
				for(int i = 0; i < predicateSize[0]; i++) {
					_domain.addPredicate(predicate[i]);
				}
			}*/
		} catch(Exception e) {
			JOptionPane.showMessageDialog(null, "Something is wrong with domain!");
		}
		// end create domain
		return _domain;
		
	}
	
	// create knowledge base
	public static FOLKnowledgeBase createFOLKnowledgeBase(InferenceProcedure infp, FOLDomain _domain, String[] rule, int[] ruleSize, String[] line, int[] lineSize, String[] plane, int[] planeSize) {
		
		FOLKnowledgeBase kb = new FOLKnowledgeBase(_domain, infp);
		FOLLexer lexer = new FOLLexer(_domain);
		FOLParser parser = new FOLParser(lexer);
		Forward_Backward_Chaining_UI_Demo.relationSize[0] = 0;
		
		try {
			kb.tell("((line_diff_line(x,y) AND plane_contain_line(z,x) AND plane_contain_line(z,y) AND line_orth_line(t,x) AND line_orth_line(t,y)) => line_orth_plane(t,z))");
			kb.tell("((line_orth_line(y,x) AND line_orth_plane(y,z)) => line_parallel_plane(x,z))");
			//kb.tell("(line_orth_line(x,y) => line_orth_line(y,x))");
		} catch(Exception e) {
			JOptionPane.showMessageDialog(null, "Something went wrong with default rule" );
			return new FOLKnowledgeBase(_domain, infp);
		}
		
		for(int i = 0; i < ruleSize[0]; i ++) {
			
			try {
				Predicate p = (Predicate) parser.parse(rule[i]);
				if(p.getPredicateName().equals("line_orth_line")) {
					Term element1 = p.getTerms().get(0);
					Boolean find1 = isElementExistInList(element1, line, lineSize, parser );
					Term element2 = p.getTerms().get(1);
					Boolean find2 = isElementExistInList(element2, line, lineSize, parser );
					if(find1 && find2) {
						Forward_Backward_Chaining_UI_Demo.relation[i][0] = element1.getSymbolicName();
						Forward_Backward_Chaining_UI_Demo.relation[i][1] = element2.getSymbolicName();
						Forward_Backward_Chaining_UI_Demo.relation[i][2] = "orthLine";
						Forward_Backward_Chaining_UI_Demo.relationSize[0] += 1;
						System.out.println(element1.getSymbolicName());
						kb.tell(rule[i]);
					}	else {
						Forward_Backward_Chaining_UI_Demo.relationSize[0] = 0;
						JOptionPane.showMessageDialog(null, "error rule    " + rule[i]);
						return new FOLKnowledgeBase(_domain, infp);
					}
						
				} else if (p.getPredicateName().equals("line_orth_plane")) {
					Term element1 = p.getTerms().get(0);
					Boolean find1 = isElementExistInList(element1, line, lineSize, parser );
					Term element2 = p.getTerms().get(1);
					Boolean find2 = isElementExistInList(element2, plane, planeSize, parser );
					if(find1 && find2) {
						Forward_Backward_Chaining_UI_Demo.relation[i][0] = element1.getSymbolicName();
						Forward_Backward_Chaining_UI_Demo.relation[i][1] = element2.getSymbolicName();
						Forward_Backward_Chaining_UI_Demo.relation[i][2] = "orthPlane";
						Forward_Backward_Chaining_UI_Demo.relationSize[0] += 1;
						System.out.println(element1.getSymbolicName());
						kb.tell(rule[i]);
					}	else {
						Forward_Backward_Chaining_UI_Demo.relationSize[0] = 0;
						JOptionPane.showMessageDialog(null, "error rule    " + rule[i]);
						return new FOLKnowledgeBase(_domain, infp);
					}
				} else if (p.getPredicateName().equals("plane_contain_line")) {
					Term element1 = p.getTerms().get(0);
					Boolean find1 = isElementExistInList(element1, plane, planeSize, parser );
					Term element2 = p.getTerms().get(1);
					Boolean find2 = isElementExistInList(element2, line, lineSize, parser );
					if(find1 && find2) {
						Forward_Backward_Chaining_UI_Demo.relation[i][0] = element1.getSymbolicName();
						Forward_Backward_Chaining_UI_Demo.relation[i][1] = element2.getSymbolicName();
						Forward_Backward_Chaining_UI_Demo.relation[i][2] = "contain";
						Forward_Backward_Chaining_UI_Demo.relationSize[0] += 1;
						System.out.println(element1.getSymbolicName());
						kb.tell(rule[i]);
					}	else {
						Forward_Backward_Chaining_UI_Demo.relationSize[0] = 0;
						JOptionPane.showMessageDialog(null, "error rule   " + rule[i]);
						return new FOLKnowledgeBase(_domain, infp);
					}
				}	else if (p.getPredicateName().equals("line_diff_line")) {
					Term element1 = p.getTerms().get(0);
					Boolean find1 = isElementExistInList(element1, line, lineSize, parser );
					Term element2 = p.getTerms().get(1);
					Boolean find2 = isElementExistInList(element2, line, lineSize, parser );
					if(find1 && find2) {
						Forward_Backward_Chaining_UI_Demo.relation[i][0] = element1.getSymbolicName();
						Forward_Backward_Chaining_UI_Demo.relation[i][1] = element2.getSymbolicName();
						Forward_Backward_Chaining_UI_Demo.relation[i][2] = "diff";
						Forward_Backward_Chaining_UI_Demo.relationSize[0] += 1;
						System.out.println(element1.getSymbolicName());
						kb.tell(rule[i]);
					}	else {
						Forward_Backward_Chaining_UI_Demo.relationSize[0] = 0;
						JOptionPane.showMessageDialog(null, "error rule" + rule[i]);
						return new FOLKnowledgeBase(_domain, infp);
					}
				}	else if (p.getPredicateName().equals("line_parallel_plane")) {
					Term element1 = p.getTerms().get(0);
					Boolean find1 = isElementExistInList(element1, line, lineSize, parser );
					Term element2 = p.getTerms().get(1);
					Boolean find2 = isElementExistInList(element2, plane, planeSize, parser );
					if(find1 && find2) {
						Forward_Backward_Chaining_UI_Demo.relation[i][0] = element1.getSymbolicName();
						Forward_Backward_Chaining_UI_Demo.relation[i][1] = element2.getSymbolicName();
						Forward_Backward_Chaining_UI_Demo.relation[i][2] = "parallel";
						Forward_Backward_Chaining_UI_Demo.relationSize[0] += 1;
						System.out.println(element1.getSymbolicName());
						kb.tell(rule[i]);
					}	else {
						Forward_Backward_Chaining_UI_Demo.relationSize[0] = 0;
						JOptionPane.showMessageDialog(null, "error rule" + rule[i]);
						return new FOLKnowledgeBase(_domain, infp);
					}
				}	else {
					JOptionPane.showMessageDialog(null, "error rule ");
					return new FOLKnowledgeBase(_domain, infp);
					
					
				}
				
			} catch(Exception e) {
				JOptionPane.showMessageDialog(null, "Something went wrong with rule " + rule[i]);
				return new FOLKnowledgeBase(_domain, infp);
			}
			
			
			
		}
		
		return kb;
		
	}
	
	// solve forward chaining and backward chaining
	public static String chainingSolve(FOLDomain domain, FOLKnowledgeBase kb, JTextField textQuery) {
		
		// FOLParser parser = new FOLParser(domain);
		
		// take query
		String kbStr = kb.toString();
		String myQuery = textQuery.getText();
		// JOptionPane.showMessageDialog(null, myQuery);
		
		
		String result = "";
		try {
			// Sentence  query = parser.parse(myQuery);
			
			InferenceResult answer = kb.ask(myQuery);
			
			//System.out.println("Knowledge Base");
			result += "Knowledge Base: " + "\n" ;
			result += kbStr + "\n";
			result += "\n";
			// System.out.println(kbStr);
			result += "Query: " + myQuery + "\n" + "\n";
			//System.out.println("Query: " + query);
			result += "result: \n";
			for (Proof p : answer.getProofs()) {
				//System.out.print(ProofPrinter.printProof(p));
				//System.out.println("");
				result += ProofPrinter.printProof(p) + "\n";
			}
			return result;
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, "wrong query");
			return "";
		}
	}
	
	
	//Solving by resolution
	public static String  resolutionSolve(FOLDomain domain, FOLKnowledgeBase kb, JTextField textQuery) {
		
		// FOLParser parser = new FOLParser(domain);
		
		// take query
		String kbStr = kb.toString();
		String myQuery = textQuery.getText().trim();
		// JOptionPane.showMessageDialog(null, myQuery);
		
		
		String result = "";
		try {
			// Sentence  query = parser.parse(myQuery);
			
			InferenceResult answer = kb.ask(myQuery);
			
			//System.out.println("Knowledge Base");
			result += "NhuanKnowledge Base: " + "\n" ;
			result += kbStr + "\n";
			result += "\n";
			// System.out.println(kbStr);
			result += "Query: " + myQuery + "\n" + "\n";
			//System.out.println("Query: " + query);
			for (Proof p : answer.getProofs()) {
				//System.out.print(ProofPrinter.printProof(p));
				//System.out.println("");
				result += ProofPrinter.printProof(p) + "\n";
			}
			return result;
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, "wrong query");
			return "";
		}
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////
	

	/**
	 * Create the frame.
	 */
	public Forward_Backward_Chaining_UI_Demo() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		
		// global variable  //////////////////////////////////////////////
		
		Arrays.fill(constant, "");
		
		constantSize[0] = 0;
		
		
		Arrays.fill(function, "");
		
		functionSize[0] = 0;
		
		
		Arrays.fill(predicate, "");
		
		predicateSize[0] = 0;
		
		
		Arrays.fill(rule, "");
		
		ruleSize[0] = 0;
		
		
		/////////////////////////////////////////////////////////////////
		
		// show output //////////////////////////////////////////////////
		// constants output
		
		final JTextPane txtpnConstant = new JTextPane();
		JScrollPane _txtScrollConstant = new JScrollPane(txtpnConstant);
		_txtScrollConstant.setBounds(25, 21, 272, 231);
		txtpnConstant.setBounds(25, 21, 272, 231);
		contentPane.add(_txtScrollConstant);
		txtpnConstant.setFont(font1);		
		// functions output
		
		final JTextPane txtpnFunction = new JTextPane();
		JScrollPane _txtScrollFunction = new JScrollPane(txtpnFunction);
		_txtScrollFunction.setBounds(372, 21, 272, 231);
		txtpnFunction.setBounds(372, 21, 272, 231);
		txtpnFunction.setFont(font1);
		contentPane.add(_txtScrollFunction);
		
		/*
		// predicates output
		final JTextPane txtpnPredicate = new JTextPane();
		JScrollPane _txtScrollPredicate = new JScrollPane(txtpnPredicate);
		_txtScrollPredicate.setBounds(690, 21, 272, 231);
		txtpnPredicate.setBounds(690, 21, 272, 231);
		contentPane.add(_txtScrollPredicate);	
		txtpnPredicate.setFont(font1);
		*/
		
		// rules output
		final JTextPane txtpnRule = new JTextPane();
		JScrollPane _txtScrollRule = new JScrollPane(txtpnRule);
		_txtScrollRule.setBounds(25, 367, 505, 196);
		txtpnRule.setBounds(25, 367, 505, 196);
		contentPane.add(_txtScrollRule);
		txtpnRule.setFont(font1);
		//////////////////////////////////////////////////////////////////////////////////
		
		
		//  constant ////////////////////////////////////////////////////
		
		// text field
		textNewConstant = new JTextField();
		textNewConstant.setFont(font1);
		textNewConstant.setBounds(74, 263, 182, 30);
		contentPane.add(textNewConstant);
		textNewConstant.setColumns(10);
		ActionListener textNewConstantAction = new  ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				// get value from text field
				String newConstant = textNewConstant.getText().trim();
				
				// insert
				Boolean isSuccess = insertElementIntoArray(constant, constantSize, newConstant);
				
				// show result
				if(isSuccess) {
					String result = "";
					for(int i = 0; i < constantSize[0]; i++) {
						result += constant[i] + "\t";
					}
					
					txtpnConstant.setText(result);
				}
				textNewConstant.setText("");
			}
		};
		//Add action listener to text field
		textNewConstant.addActionListener(textNewConstantAction);
		// add button
		JButton btnAddConstant = new JButton("ADD LINE");
		btnAddConstant.addActionListener(textNewConstantAction);
		btnAddConstant.setBounds(10, 304, 140, 30);
		contentPane.add(btnAddConstant);
		
		// remove button
		JButton btnRemoveConstant = new JButton("REMOVE LINE");
		btnRemoveConstant.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				// get value from text field
				String newConstant = textNewConstant.getText().trim();
				
				// remove
				Boolean isSuccess = removeElementOutOfArray(constant, constantSize, newConstant);
				
				// show result
				if(isSuccess) {
					String result = "";
					for(int i = 0; i < constantSize[0]; i++) {
						result += constant[i] + "\t";
					}
					
					txtpnConstant.setText(result);
				}
				textNewConstant.setText("");

			}
		});
		btnRemoveConstant.setBounds(160, 304, 161, 30);
		contentPane.add(btnRemoveConstant);
		
		
		///////////////////////////////////////////////////////////////////
		final GLProfile profile = GLProfile.get( GLProfile.GL2 );
	      GLCapabilities capabilities = new GLCapabilities( profile );
	      
	      // The canvas
	      GLJPanel gljPanel = new GLJPanel(capabilities);
	      Cube cube = new Cube();
			
	      gljPanel.addGLEventListener( cube );
	      gljPanel.setSize( 300, 300 );
	      gljPanel.setBounds(650, 10, 300, 300);
	      contentPane.add(gljPanel);
	      FPSAnimator animator = new FPSAnimator(gljPanel,300,true);
	      animator.start();
		/*
		//  predicate /////////////////////////////////////////////////////
		
		// text field
		textNewPredicate = new JTextField();
		textNewPredicate.setFont(font1);
		textNewPredicate.setColumns(10);
		textNewPredicate.setBounds(736, 263, 182, 30);
		contentPane.add(textNewPredicate);
		ActionListener textNewPreAction = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// get value from text field
				String newPredicate = textNewPredicate.getText().trim();
				
				// insert
				Boolean isSuccess = insertElementIntoArray(predicate, predicateSize, newPredicate);
				
				// show result
				if(isSuccess) {
					String result = "";
					for(int i = 0; i < predicateSize[0]; i++) {
						result += predicate[i] + "\t";
					}
					
					txtpnPredicate.setText(result);
				}
				textNewPredicate.setText("");
			}
		};
		//
		textNewPredicate.addActionListener(textNewPreAction);
		// add button
		JButton btnAddPredicate = new JButton("ADD PREDICATE");
		btnAddPredicate.addActionListener(textNewPreAction);
		btnAddPredicate.setBounds(670, 304, 133, 30);
		contentPane.add(btnAddPredicate);
		
		// remove button
		JButton btnRemovePredicate = new JButton("REMOVE PREDICATE");
		btnRemovePredicate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// get value from text field
				String newPredicate = textNewPredicate.getText().trim();
				
				// remove
				Boolean isSuccess = removeElementOutOfArray(predicate, predicateSize, newPredicate);
						
				// show result
				if(isSuccess) {
					String result = "";
					for(int i = 0; i < predicateSize[0]; i++) {
						result += predicate[i] + "\t";
					}
					
					txtpnPredicate.setText(result);
				}
				textNewPredicate.setText("");
			}
		});
		btnRemovePredicate.setBounds(813, 304, 161, 30);
		contentPane.add(btnRemovePredicate);
				
		/////////////////////////////////////////////////////////////////////
		*/
		
		//  function ///////////////////////////////////////////////////////
		// text field
		textNewFunction = new JTextField();
		textNewFunction.setFont(font1);
		textNewFunction.setColumns(10);
		textNewFunction.setBounds(419, 263, 182, 30);
		contentPane.add(textNewFunction);
		ActionListener newFunctionAction = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// get value from text field
				String newFunction = textNewFunction.getText().trim();
				
				// insert
				Boolean isSuccess = insertElementIntoArray(function, functionSize, newFunction);
				
				// show result
				if(isSuccess) {
					String result = "";
					for(int i = 0; i < functionSize[0]; i++) {
						result += function[i] + "\t";
					}
					
					txtpnFunction.setText(result);
				}
				textNewFunction.setText("");
			}
		};
		
		//
		textNewFunction.addActionListener(newFunctionAction);
		// add button
		
		
		JButton btnAddFunction = new JButton("ADD PLANE");
		btnAddFunction.addActionListener(newFunctionAction);
		btnAddFunction.setBounds(356, 304, 133, 30);
		contentPane.add(btnAddFunction);

		// remove button

		JButton btnRemoveFunction = new JButton("REMOVE PLANE");
		btnRemoveFunction.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// get value from text field
				String newFunction = textNewFunction.getText().trim();
				
				// remove
				Boolean isSuccess = removeElementOutOfArray(function, functionSize, newFunction);
				
				// show result
				if(isSuccess) {
					String result = "";
					for(int i = 0; i < functionSize[0]; i++) {
						result += function[i] + "\t";
					}
					
					txtpnFunction.setText(result);
				}
				textNewFunction.setText("");
			}
		});
		btnRemoveFunction.setBounds(496, 304, 148, 30);
		contentPane.add(btnRemoveFunction);
		

		//////////////////////////////////////////////////////////////
		
		//  rule ///////////////////////////////////////////////////////
		
		// text field
		textNewRule = new JTextField();
		textNewRule.setFont(font1);
		textNewRule.setColumns(10);
		textNewRule.setBounds(25, 574, 505, 30);
		contentPane.add(textNewRule);
		ActionListener newRuleAction = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// get value from text field
				String newRule = textNewRule.getText().trim();
				
				// insert
				Boolean isSuccess = insertElementIntoArray(rule, ruleSize, newRule);
				
				
				// show result
				if(isSuccess) {
					String result = "";
					for(int i = 0; i < ruleSize[0]; i++) {
						result += rule[i] + "\n";
					}
					txtpnRule.setText(result);
					// create domain from constant, function and predicate
					FOLDomain domain = createDomain(constant, function, predicate, constantSize, functionSize, predicateSize);
					
					InferenceProcedure infp = new FOLTFMResolution();
					// create a fol knowledge base
					FOLKnowledgeBase kb = createFOLKnowledgeBase(infp, domain, rule, ruleSize, constant, constantSize, function, functionSize);
					
					
				}
				
			
				textNewRule.setText("");
			}
		};
		
		textNewRule.addActionListener(newRuleAction);
		// add button
		JButton btnAddRule = new JButton("ADD RULE");
		btnAddRule.addActionListener(newRuleAction);
		btnAddRule.setBounds(74, 620, 182, 30);
		contentPane.add(btnAddRule);
		
		// remove button
		JButton btnRemoveRule = new JButton("REMOVE RULE");
		btnRemoveRule.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// get value from text field
				String newRule = textNewRule.getText().trim();
				
				// insert
				Boolean isSuccess = removeElementOutOfArray(rule, ruleSize, newRule);
				
				// show result
				if(isSuccess) {
					String result = "";
					for(int i = 0; i < ruleSize[0]; i++) {
						result += rule[i] + "\n";
					}
					
					txtpnRule.setText(result);
				}
				textNewRule.setText("");
			}
		});
		btnRemoveRule.setBounds(276, 620, 182, 30);
		contentPane.add(btnRemoveRule);
		///////////////////////////////////////////////////////////////
		
		
		// Query text label
		JLabel lblQuery = new JLabel("Query");
		lblQuery.setBounds(584, 445, 48, 30);
		contentPane.add(lblQuery);
		
		// Query text field
		textQuery = new JTextField();
		textQuery.setFont(font1);
		textQuery.setColumns(10);
		textQuery.setBounds(642, 445, 297, 30);
		contentPane.add(textQuery);
		
		// click event to solve and illustrate result
		JButton btnForwardChaining = new JButton("FORWARD CHAINING");
		btnForwardChaining.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				// create domain from constant, function and predicate
				FOLDomain domain = createDomain(constant, function, predicate, constantSize, functionSize, predicateSize);
				
				InferenceProcedure infp = new FOLFCAsk();
				// create a fol knowledge base
				FOLKnowledgeBase kb = createFOLKnowledgeBase(infp, domain, rule, ruleSize, constant, constantSize, function, functionSize);
				
				
				// chainingSolve(infp, domain, kb, textQuery);
				String title = "Forward chaining \n \n";
				try {
					ResultWindow frame = new ResultWindow(title + chainingSolve(domain, kb, textQuery));
					frame.setVisible(true);
				} catch (Exception k) {
					k.printStackTrace();
				}
				
			}
		});
		btnForwardChaining.setBounds(605, 486, 161, 30);
		contentPane.add(btnForwardChaining);
	
		
		JButton btnBackwardChaining = new JButton("BACKWARD CHAINING");
		btnBackwardChaining.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// create domain from constant, function and predicate
				FOLDomain domain = createDomain(constant, function, predicate, constantSize, functionSize, predicateSize);
				
				InferenceProcedure infp = new FOLBCAsk();
				// create a fol knowledge base
				FOLKnowledgeBase kb = createFOLKnowledgeBase(infp, domain, rule, ruleSize, constant, constantSize, function, functionSize);
				/*
				// after create domain
				System.out.println("here");
				String kbStr = kb.toString();
				System.out.println(kbStr);
				System.out.println("here");
				*/
				
				//chainingSolve(infp, domain, kb, textQuery);
				String title = "Backward chaining \n \n";
				try {
					ResultWindow frame = new ResultWindow(title + chainingSolve(domain, kb, textQuery));
					frame.setVisible(true);
				} catch (Exception k) {
					k.printStackTrace();
				}
			}
		});
		btnBackwardChaining.setBounds(786, 486, 176, 30);
		contentPane.add(btnBackwardChaining);
		
		
		
		///////////////////////////////////////////////////////////////
		
		// click event to solve and illustrate result
				JButton btnSolve = new JButton("SOLVE");
				btnSolve.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
					
						// create domain from constant, function and predicate
						FOLDomain domain = createDomain(constant, function, predicate, constantSize, functionSize, predicateSize);
						
						InferenceProcedure infp = new FOLTFMResolution();
						// create a fol knowledge base
						FOLKnowledgeBase kb = createFOLKnowledgeBase(infp, domain, rule, ruleSize, constant, constantSize, function, functionSize);
						
						
						// chainingSolve(infp, domain, kb, textQuery);
						String title = "Resolution \n \n";
						try {
							ResultWindow frame = new ResultWindow(title + resolutionSolve(domain, kb, textQuery));
							frame.setVisible(true);
						} catch (Exception k) {
							k.printStackTrace();
						}
						
					}
				});
				btnSolve.setBounds(605, 550, 161, 30);
				contentPane.add(btnSolve);
		/////////////////////////////////////////////////////////////
				JButton btnUnifier = new JButton("Unifier");
				btnUnifier.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
					
						// create domain from constant, function and predicate
						FOLDomain domain = createDomain(constant, function, predicate, constantSize, functionSize, predicateSize);
						
						InferenceProcedure infp = new FOLTFMResolution();
						// create a fol knowledge base
						FOLKnowledgeBase kb = createFOLKnowledgeBase(infp, domain, rule, ruleSize, constant, constantSize, function, functionSize);
						
						
						// chainingSolve(infp, domain, kb, textQuery);
						String title = "Forward chaining \n \n";
						try {
							Unifier_UI_Demo frame = new Unifier_UI_Demo(domain);
							frame.setVisible(true);
						} catch (Exception k) {
							k.printStackTrace();
						}
						
					}
				});
				btnUnifier.setBounds(786, 550, 161, 30);
				contentPane.add(btnUnifier);
				////////////////////////
				JButton btnNew = new JButton("New");
				btnNew.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
					
						constantSize[0]=0;
						txtpnConstant.setText("");
						predicateSize[0]=0;	
						//txtpnPredicate.setText("");
						ruleSize[0]=0;
						txtpnRule.setText("");
						functionSize[0]=0;
						txtpnFunction.setText("");
						}
				});
				btnNew.setBounds(786, 600, 161, 30);
				contentPane.add(btnNew);
	}
}

