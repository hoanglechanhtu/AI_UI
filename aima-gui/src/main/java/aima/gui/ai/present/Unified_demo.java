package aima.gui.ai.present;

import java.util.Hashtable;
import java.util.Map;

import aima.core.logic.fol.Unifier;
import aima.core.logic.fol.domain.DomainFactory;
import aima.core.logic.fol.parsing.FOLParser;
import aima.core.logic.fol.parsing.ast.Sentence;
import aima.core.logic.fol.parsing.ast.Term;
import aima.core.logic.fol.parsing.ast.Variable;

public class Unified_demo {
	public static void main(String[] args) {
		unifierDemo();
	}
	private static void unifierDemo() {
		FOLParser parser = new FOLParser(DomainFactory.knowsDomain());
		
		Unifier unifier = new Unifier();
		Map<Variable, Term> theta = new Hashtable<Variable, Term>();

		Sentence query = parser.parse("Knows(John,x)");
		Sentence johnKnowsJane = parser.parse("Knows(y,Mother(y))");
		
		
		System.out.println("------------");
		System.out.println("Unifier Demo");
		System.out.println("------------");
		Map<Variable, Term> subst = unifier.unify(query, johnKnowsJane, theta);
		
		System.out.println("Unify '" + query + "' with '" + johnKnowsJane
				+ "' to get the substitution " + subst + ".");
		System.out.println(theta);
		System.out.println("");
	}
}
