package com.hoanglechanhtu.mavenproject;

import com.jogamp.opengl.GL2;


import com.jogamp.opengl.GLAutoDrawable;
 
import com.jogamp.opengl.GLCapabilities;
 
import com.jogamp.opengl.GLEventListener;
 
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.awt.GLJPanel;
import com.jogamp.opengl.glu.GLU;

import java.awt.DisplayMode;
import java.awt.ItemSelectable;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.text.Position;

import com.jogamp.opengl.util.FPSAnimator;




public class Cube implements GLEventListener {
	
   public static DisplayMode dm, dm_old;
   private GLU glu = new GLU();
 
   private float[][] Position = new float[][]{{-1.2f,-(float)Math.random(),-0.7f},{-1.3f,-(float)Math.random(),0.7f},{-(float)Math.random(),-(float)Math.random(),-(float)Math.random()},{-(float)Math.random(),-(float)Math.random(),-(float)Math.random()},{-(float)Math.random(),-(float)Math.random(),-(float)Math.random()},{-(float)Math.random(),-(float)Math.random(),-(float)Math.random()},{-(float)Math.random(),-(float)Math.random(),-(float)Math.random()},{-(float)Math.random(),-(float)Math.random(),-(float)Math.random()},{-(float)Math.random(),-(float)Math.random(),-(float)Math.random()},{-(float)Math.random(),-(float)Math.random(),-(float)Math.random()}};
   private float[][] Vector = new float[][]{{0.7f,-1+2*(float)Math.random(),0.3f},{0.8f,-1+2*(float)Math.random(),-0.6f},{-1+2*(float)Math.random(),-1+2*(float)Math.random(),-1+2*(float)Math.random()},{-1+2*(float)Math.random(),-1+2*(float)Math.random(),-1+2*(float)Math.random()},{-1+2*(float)Math.random(),-1+2*(float)Math.random(),-1+2*(float)Math.random()},{-1+2*(float)Math.random(),-1+2*(float)Math.random(),-1+2*(float)Math.random()},{-1+2*(float)Math.random(),-1+2*(float)Math.random(),-1+2*(float)Math.random()},{-1+2*(float)Math.random(),-1+2*(float)Math.random(),-1+2*(float)Math.random()},{-1+2*(float)Math.random(),-1+2*(float)Math.random(),-1+2*(float)Math.random()},{-1+2*(float)Math.random(),-1+2*(float)Math.random(),-1+2*(float)Math.random()}};
   private String[] planes = new String[]{"p"};
   private String[] lines = new String[]{"l1","l2","l3","l4"};
   private String[][] a = new String[][]{{"p","l1","contain"},{"p","l2","contain"},{"l3","l1","orthLine"},{"l3","l2","orthLine"},{"l4","l3","orthLine"}};
  
   
   List<_3dObject> items= new ArrayList<_3dObject>();
   
 
   private float rquad = 0.0f - 30f;
   
   private void drawLine(GL2 gl, float point[], float pointVector[], float color[]) {
	   
	   
	   if(pointVector.length != 3 || point.length != 3 || color.length != 3) return;
	   // calculate point
	   if(point[0] != 0) {
		   point[1] = point[1];
	   }
	   gl.glBegin(GL2.GL_LINES);
	   gl.glLineWidth(4);
	   gl.glColor3f(color[0],color[1],color[2]);
	   // calculate point 
	   
	   gl.glEnd();
	   /*
	   gl.glBegin(GL2.GL_LINES);
	   gl.glLineWidth(4);
	   gl.glColor3f(1f,0f,0f);
	   gl.glVertex3f(1.0f,  -1.0f, 1.0f);
	   gl.glVertex3f(1.0f,  1.0f, 1.0f);
	   gl.glEnd();
	   */
   }
   
   private void drawPlane(GL2 gl) {
	   gl.glBegin(GL2.GL_QUADS);
	   gl.glLineWidth(4);
	   gl.glColor3f(1f,0f,0f);
	   gl.glVertex3f(-1.0f,  -1.0f, 1.0f);
	   gl.glVertex3f(1.0f,  -1.0f, 1.0f);
	   gl.glVertex3f(1.0f,  1.0f, 1.0f);
	   gl.glVertex3f(-1.0f,  1.0f, 1.0f);
	   gl.glEnd();
	   
   }
 
   
   public void display( GLAutoDrawable drawable ) {
	   items.clear();
	   items.add(new Plane("p"));

	  for(int i =0; i <Forward_Backward_Chaining_UI_Demo.constantSize[0];i++ ){
			 
			  items.add(new Line(Forward_Backward_Chaining_UI_Demo.constant[i]));
		  }
		
	  int j=0;
	  for (_3dObject item : items) {
		item.r=0;
	  }
	  for(int i=0; i<Forward_Backward_Chaining_UI_Demo.relationSize[0];i++){
		  if(Forward_Backward_Chaining_UI_Demo.relation[i][2].equals("contain")){
			  Plane tempPlane=null;
			  Line tempLine=null;
			  for (_3dObject item : items) {
				if(Forward_Backward_Chaining_UI_Demo.relation[i][0].equals(item.name))
					tempPlane = (Plane)item;
				if(Forward_Backward_Chaining_UI_Demo.relation[i][1].equals(item.name))
					tempLine = (Line)item;
			  }
			  tempLine.x =Position[j][0];
			  tempLine.y = -1;
			  tempLine.z = Position[j][2];
			  tempLine.a=Vector[j][0];
			  tempLine.b =  0;
			  tempLine.c =Vector[j][2];
			  if(tempLine.r<=3){
				  tempLine.orth[tempLine.r][0] = 0;
				  tempLine.orth[tempLine.r][1]=1;
				  tempLine.orth[tempLine.r][2]=0;
				  tempLine.r+=1;
				  
			  }
			   
			  j++;
		  }
		  if(Forward_Backward_Chaining_UI_Demo.relation[i][2].equals("orthLine")){
			  Line tempLine=null;
			  Line tempLine1=null;
			  for (_3dObject item : items) {
				if(Forward_Backward_Chaining_UI_Demo.relation[i][0].equals(item.name))
					tempLine = (Line)item;
				if(Forward_Backward_Chaining_UI_Demo.relation[i][1].equals(item.name))
					tempLine1 = (Line)item;
			  }
			 if(tempLine.r<=tempLine1.r){
				 Line temp= tempLine;
				 tempLine =  tempLine1;
				 tempLine1 = temp;
				 
			 }
			 if(tempLine1.r<3){
				 tempLine1.orth[tempLine1.r][0]=tempLine.a;
				 tempLine1.orth[tempLine1.r][1]=tempLine.b;
				 tempLine1.orth[tempLine1.r][2]=tempLine.c;
				 tempLine1.r++;
			 }
			 tempLine1.calculateD();
			  if(Forward_Backward_Chaining_UI_Demo.relation[i][0].equals("l4"))
				  System.out.println(tempLine1.a);

			 tempLine1.x = tempLine.x +tempLine.a -tempLine1.a*0.5f;
			 tempLine1.y = tempLine.y +tempLine.b-tempLine1.b*0.5f;
			 tempLine1.z = tempLine.z +tempLine.c-tempLine1.c*0.5f;
		  }
		  if(Forward_Backward_Chaining_UI_Demo.relation[i][2].equals("parallel")){
			  Plane tempPlane=null;
			  Line tempLine=null;
			  for (_3dObject item : items) {
					if(Forward_Backward_Chaining_UI_Demo.relation[i][0].equals(item.name))
						tempPlane = (Plane)item;
					if(Forward_Backward_Chaining_UI_Demo.relation[i][1].equals(item.name))
						tempLine = (Line)item;
				  }
			  tempLine.x = -1.3f;
			  tempLine.y = -1.2f;
			  tempLine.z = -0.9f;
			  
			  tempLine.a = 0.8f;
			  tempLine.b = 0;
			  tempLine.c = 0.3f;
		  }
	  } 
	  
      final GL2 gl = drawable.getGL().getGL2();
      gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT );
      
	  gl.glLoadIdentity();
	  gl.glTranslatef( 0f, 0f, -5.0f ); 
   	  for (_3dObject item : items) {
    
   		  item.draw(drawable);
	  }
   	  gl.glPopMatrix();
 
 
      gl.glFlush();
		
      rquad -= 0.15f;
   }
   
   public void dispose( GLAutoDrawable drawable ) {
      // TODO Auto-generated method stub
   }
   
   public void init( GLAutoDrawable drawable ) {
	 
	  
	  
      final GL2 gl = drawable.getGL().getGL2();
      gl.glShadeModel( GL2.GL_SMOOTH );
      gl.glClearColor( 1f, 1f, 1f, 1f );
      gl.glClearDepth( 1.0f );
      gl.glEnable( GL2.GL_DEPTH_TEST );
      gl.glDepthFunc( GL2.GL_LEQUAL );
      gl.glHint( GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL2.GL_NICEST );
   }
      
   public void reshape( GLAutoDrawable drawable, int x, int y, int width, int height ) {
	
      // TODO Auto-generated method stub
      final GL2 gl = drawable.getGL().getGL2();
      if( height <= 0 )
         height = 1;
			
      final float h = ( float ) width / ( float ) height;
      gl.glViewport( 0, 0, width, height );
      gl.glMatrixMode( GL2.GL_PROJECTION );
      gl.glLoadIdentity();
		
      glu.gluPerspective( 45.0f, h, 1.0, 20.0 );
      gl.glMatrixMode( GL2.GL_MODELVIEW );
      gl.glLoadIdentity();
   }
   public void drawCube() {
	   final GLProfile profile = GLProfile.get( GLProfile.GL2 );
	   
	   	
	      GLCapabilities capabilities = new GLCapabilities( profile );
	      
	      // The canvas
	      final GLCanvas glcanvas = new GLCanvas( capabilities );
	      Cube cube = new Cube();
			
	      glcanvas.addGLEventListener( cube );
	      glcanvas.setSize( 400, 400 );
			
	      final JFrame frame = new JFrame ( " Multicolored cube" );
	      frame.getContentPane().add( glcanvas );
	      frame.setSize( frame.getContentPane().getPreferredSize() );
	      frame.setVisible( true );
	      final FPSAnimator animator = new FPSAnimator(glcanvas, 300,true);
			
	      animator.start();   
   }
   public static void main( String[] args ) {
	  
      final GLProfile profile = GLProfile.get( GLProfile.GL2 );
      GLCapabilities capabilities = new GLCapabilities( profile );
      
      // The canvas
      final GLCanvas glcanvas = new GLCanvas( capabilities );
      Cube cube = new Cube();
		
      glcanvas.addGLEventListener( cube );
      glcanvas.setSize( 400, 400 );
		
      final JFrame frame = new JFrame ( " Multicolored cube" );
      frame.getContentPane().add( glcanvas );
      frame.setSize( frame.getContentPane().getPreferredSize() );
      frame.setVisible( true );
      final FPSAnimator animator = new FPSAnimator(glcanvas, 300,true);
		
      animator.start();
   }
   
   ////////////////////// here
  }