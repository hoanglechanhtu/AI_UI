package aima.gui.ai.present;

import java.util.ArrayList;
import java.util.List;

import aima.core.logic.fol.StandardizeApartIndexicalFactory;
import aima.core.logic.fol.inference.FOLTFMResolution;
import aima.core.logic.fol.inference.InferenceProcedure;
import aima.core.logic.fol.inference.InferenceResult;
import aima.core.logic.fol.inference.proof.Proof;
import aima.core.logic.fol.inference.proof.ProofPrinter;
import aima.core.logic.fol.kb.FOLKnowledgeBase;
import aima.core.logic.fol.kb.FOLKnowledgeBaseFactory;
import aima.core.logic.fol.parsing.ast.Constant;
import aima.core.logic.fol.parsing.ast.Predicate;
import aima.core.logic.fol.parsing.ast.Term;
import aima.core.logic.fol.parsing.ast.TermEquality;
import aima.core.logic.fol.parsing.ast.Variable;
public class ResolutionDemo {
	public static void main(String[] args) {
		fOL_TFMResolutionDemo();
	}
	private static void fOL_TFMResolutionDemo() {
		System.out.println("----------------------------");
		System.out.println("TFM Resolution, Kings Demo 1");
		System.out.println("----------------------------");
		kingsDemo1(new FOLTFMResolution());
		System.out.println("----------------------------");
		System.out.println("TFM Resolution, Kings Demo 2");
		System.out.println("----------------------------");
		kingsDemo2(new FOLTFMResolution());
		System.out.println("----------------------------");
		System.out.println("TFM Resolution, Weapons Demo");
		System.out.println("----------------------------");
		weaponsDemo(new FOLTFMResolution());
		System.out.println("---------------------------------");
		System.out.println("TFM Resolution, Loves Animal Demo");
		System.out.println("---------------------------------");
		lovesAnimalDemo(new FOLTFMResolution());
		System.out.println("---------------------------------------");
		System.out.println("TFM Resolution, ABC Equality Axiom Demo");
		System.out.println("---------------------------------------");
		abcEqualityAxiomDemo(new FOLTFMResolution());
	}
	
	private static void kingsDemo1(InferenceProcedure ip) {
		StandardizeApartIndexicalFactory.flush();

		FOLKnowledgeBase kb = FOLKnowledgeBaseFactory
				.createKingsKnowledgeBase(ip);

		String kbStr = kb.toString();

		List<Term> terms = new ArrayList<Term>();
		terms.add(new Constant("John"));
		Predicate query = new Predicate("Evil", terms);

		InferenceResult answer = kb.ask(query);

		System.out.println("Kings Knowledge Base:");
		System.out.println(kbStr);
		System.out.println("Query: " + query);
		for (Proof p : answer.getProofs()) {
			System.out.print(ProofPrinter.printProof(p));
			System.out.println("");
		}
	}

	private static void kingsDemo2(InferenceProcedure ip) {
		StandardizeApartIndexicalFactory.flush();

		FOLKnowledgeBase kb = FOLKnowledgeBaseFactory
				.createKingsKnowledgeBase(ip);

		String kbStr = kb.toString();

		List<Term> terms = new ArrayList<Term>();
		terms.add(new Variable("x"));
		Predicate query = new Predicate("King", terms);

		InferenceResult answer = kb.ask(query);

		System.out.println("Kings Knowledge Base:");
		System.out.println(kbStr);
		System.out.println("Query: " + query);
		for (Proof p : answer.getProofs()) {
			System.out.print(ProofPrinter.printProof(p));
		}
	}

	private static void weaponsDemo(InferenceProcedure ip) {
		StandardizeApartIndexicalFactory.flush();

		FOLKnowledgeBase kb = FOLKnowledgeBaseFactory
				.createWeaponsKnowledgeBase(ip);

		String kbStr = kb.toString();

		List<Term> terms = new ArrayList<Term>();
		terms.add(new Variable("x"));
		Predicate query = new Predicate("Criminal", terms);

		InferenceResult answer = kb.ask(query);

		System.out.println("Weapons Knowledge Base:");
		System.out.println(kbStr);
		System.out.println("Query: " + query);
		for (Proof p : answer.getProofs()) {
			System.out.print(ProofPrinter.printProof(p));
			System.out.println("");
		}
	}

	private static void lovesAnimalDemo(InferenceProcedure ip) {
		StandardizeApartIndexicalFactory.flush();

		FOLKnowledgeBase kb = FOLKnowledgeBaseFactory
				.createLovesAnimalKnowledgeBase(ip);

		String kbStr = kb.toString();

		List<Term> terms = new ArrayList<Term>();
		terms.add(new Constant("Curiosity"));
		terms.add(new Constant("Tuna"));
		Predicate query = new Predicate("Kills", terms);

		InferenceResult answer = kb.ask(query);

		System.out.println("Loves Animal Knowledge Base:");
		System.out.println(kbStr);
		System.out.println("Query: " + query);
		for (Proof p : answer.getProofs()) {
			System.out.print(ProofPrinter.printProof(p));
			System.out.println("");
		}
	}

	private static void abcEqualityAxiomDemo(InferenceProcedure ip) {
		StandardizeApartIndexicalFactory.flush();

		FOLKnowledgeBase kb = FOLKnowledgeBaseFactory
				.createABCEqualityKnowledgeBase(ip, true);

		String kbStr = kb.toString();

		TermEquality query = new TermEquality(new Constant("A"), new Constant(
				"C"));

		InferenceResult answer = kb.ask(query);

		System.out.println("ABC Equality Axiom Knowledge Base:");
		System.out.println(kbStr);
		System.out.println("Query: " + query);
		for (Proof p : answer.getProofs()) {
			System.out.print(ProofPrinter.printProof(p));
			System.out.println("");
		}
	}

	
}
