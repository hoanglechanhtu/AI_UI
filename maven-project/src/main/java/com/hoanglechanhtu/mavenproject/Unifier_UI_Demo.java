package com.hoanglechanhtu.mavenproject;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.util.Arrays;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.JScrollBar;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.Box;

//////// FOL package
import java.util.Hashtable;
import java.util.Map;

import aima.core.logic.fol.Unifier;
import aima.core.logic.fol.domain.DomainFactory;
import aima.core.logic.fol.domain.FOLDomain;
import aima.core.logic.fol.parsing.FOLParser;
import aima.core.logic.fol.parsing.ast.Sentence;
import aima.core.logic.fol.parsing.ast.Term;
import aima.core.logic.fol.parsing.ast.Variable;

public class Unifier_UI_Demo extends JFrame {

	private JPanel contentPane;
	private Font font1 = new Font("SansSerif", Font.BOLD, 20);
	private JTextField textQueryA;
	private JTextField textQueryB;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Unifier_UI_Demo frame = new Unifier_UI_Demo(new FOLDomain());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	// support function ///////////////////////////////////////////////////////////////////////////
	
	 
	/**
	 * Create the frame.
	 */
	public Unifier_UI_Demo(final FOLDomain domain) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		setBounds(100, 100, 1000, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		
		 
		// query and unifier result/////////////////////////////////////
		textQueryA = new JTextField();
		textQueryA.setColumns(10);
		textQueryA.setBounds(115, 100, 307, 30);
		textQueryA.setFont(font1);
		contentPane.add(textQueryA);
		
		textQueryB = new JTextField();
		textQueryB.setColumns(10);
		textQueryB.setBounds(115, 180, 307, 30);
		textQueryB.setFont(font1);
		contentPane.add(textQueryB);
		
		JLabel lblQueryA = new JLabel("Query A");
		lblQueryA.setBounds(36, 100, 53, 30);
		contentPane.add(lblQueryA);
		
		JLabel lblQueryB = new JLabel("Query B");
		lblQueryB.setBounds(36, 180, 53, 30);
		contentPane.add(lblQueryB);
		
		// show result element
		final JTextPane txtpnUnifierResult = new JTextPane();
		txtpnUnifierResult.setBounds(519, 100, 272, 59);
		txtpnUnifierResult.setFont(font1);
		contentPane.add(txtpnUnifierResult);
		
		// click event to solve and illustrate result
		JButton btnUnifierResult = new JButton("UNIFIER RESULT");
		btnUnifierResult.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				// create domain from constant, function and predicate
				 
				
				FOLParser parser = new FOLParser(domain);
				
				Unifier unifier = new Unifier();
				Map<Variable, Term> theta = new Hashtable<Variable, Term>();
				
				String queryAValue = textQueryA.getText();
				String queryBValue = textQueryB.getText();
				
				Sentence queryA = parser.parse(queryAValue);
				Sentence queryB = parser.parse(queryBValue);
				
				
				// show result
				Map<Variable, Term> subst = unifier.unify(queryA, queryB, theta);
				String result = subst.toString();
				txtpnUnifierResult.setText(result);
			}
		});
		btnUnifierResult.setBounds(574, 180, 161, 30);
		contentPane.add(btnUnifierResult);
		
		 
		
		///////////////////////////////////////////////////////////////
		
	}
}
